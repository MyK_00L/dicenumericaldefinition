# DiceNumericalDefinition

Android app to save and evaluate expressions containing dice throws.
Designed for D&D and other tabletop RPGs.

[Available on Google Play](https://play.google.com/store/apps/details?id=com.DiceNumericalDefinition)

[And on gitlab](https://gitlab.com/MyK_00L/dicenumericaldefinition/-/releases)

# Table of contents
* [Usage](#usage)
* [Examples](#examples)
* [Operators](#operators)

## Usage
* To add an entry simply edit an empty text box (there will always be one)
* To remove an entry, emty its text box (that will be removed once you restart the app)
* To evaluate an entry, tap the button next to it, where the result or a relevant error will be displayed

## Examples
* "[hit die] d20+7" Throws a d20 and adds 7
* "[hit die advantage] max(d20,d20)+7" takes the maximum of 2 d20 throws and adds 7
* "[rapier sneak attack] d8+2d6+4" throws a d8, 2 d6 and adds 4
* "[survive] d20>=10" throws a d20 and checks if the result is greater or equal to 10
* "[check randomness] (100000d20)/100000" the result should be about the average d20 throw (10.5)
* "[too large] fact(fib(12))" calculates the factorial of the 12th fibonacci number, since the answer is too large, the given result is positive infinity

## Operators
#### Normal operators
|name|symbol|priority|
|---|---|---|
|logic or|\|\||1|
|logic and|&&|1|
|not equal to|!=|2|
|equal to|==|2|
|greater or equal to|>=|2|
|less or equal to|<=|2|
|greater than|>|2|
|less than|<|2|
|subtraction|-|3|
|addition|+|3|
|elevation|^|4|
|division|/|4|
|multiplication|*|4|
|dice throw|d|5|
|die throw|d|6|
|logic not|!|6|
|unary minus|-|6|
|unary plus|+|6|
#### Special operators
|name|symbol|priority|
|---|---|---|
|comma|,|0|
|open parenthesis|(|-|
|closed parenthesis|)|-|
|open comment|[|-|
|closed comment|]|-|
#### Normal functions
|name|symbol|priority|
|---|---|---|
|fibonacci|fib(|7|
|factorial|fact(|7|
|round|round(|7|
|ceil|ceil(|7|
|floor|floor(|7|
|square root|sqrt(|7|
#### Multiple variable functions
|name|symbol|priority|
|---|---|---|
|min|min(|7?|
|max|max(|7?|
