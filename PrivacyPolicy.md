# Privacy Policy for DiceNumericalDefinition
This app saves the strings you put in the boxes meant for expressions in a single plaintext file in internal storage.

The file it saves these strings in should not be accessible by any other app without root permission.

This app does not and never will collect any other data, or send the strings you save to anyone.

Still, don't put your passwords in there, please.
