#!/bin/bash

SDK="/opt/android-sdk"
BTOOLS="$SDK/build-tools/34.0.0"
PLATFORM="$SDK/platforms/android-34/android.jar"
#AAPT2="$BTOOLS/aapt2"
AAPT="$BTOOLS/aapt"
D8="$BTOOLS/d8"
ZIPALIGN="$BTOOLS/zipalign"
#APKSIGNER="$BTOOLS/apksigner"
PROGUARD="proguard"
APPNAME="DiceNumericalDefinition"
MAINPATH="com/$APPNAME"
MAINCLASS="com.$APPNAME.$APPNAME"
MAINJAVA="src/$MAINPATH/$APPNAME.java"

# prepare folders
rm -rf build
mkdir -p build/gen build/obj build/objpg build/apk

echo "aapt"
# generate R.java
$AAPT package -f -m -J build/gen/ -S res -M AndroidManifest.xml -I "$PLATFORM"

echo "javac"
# generate classes
javac -bootclasspath "$JAVA_HOME/lib/rt.jar" -classpath "$PLATFORM" -d build/obj "build/gen/$MAINPATH/R.java" $MAINJAVA -source 1.8 -target 1.8

echo "proguard"
# shrink/optimize/obfuscate classes
$PROGUARD -injars build/obj -outjars build/objpg -libraryjars $PLATFORM -include proguardrules.txt -keep class $MAINCLASS

echo "d8"
# generate dex
$D8 --classpath $PLATFORM --output build/apk/ build/objpg/$MAINPATH/*.class --release

echo "aapt"
# generate apk
$AAPT package -f -M AndroidManifest.xml -S res/ -I "$PLATFORM" -F build/a0.apk build/apk/

echo "zipalign"
# align apk
$ZIPALIGN -f -p 4 build/a0.apk build/a1.apk

echo "sign"
# sign apk
#$APKSIGNER sign --ks key.keystore --ks-pass pass:keystore --out build/a2.apk build/a1.apk

