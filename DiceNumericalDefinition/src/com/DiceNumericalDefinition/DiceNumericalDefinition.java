package com.DiceNumericalDefinition;

import android.os.Bundle;
import android.app.Activity;
import android.widget.ScrollView;
import android.widget.LinearLayout;
import android.widget.Button;
import android.widget.EditText;
import android.text.InputType;
import android.text.TextWatcher;
import android.text.Editable;
import android.view.Gravity;
import android.util.DisplayMetrics;
import android.view.View;
import android.view.View.OnClickListener;

import java.util.Random;
import java.util.Vector;
import java.text.DecimalFormat;
import java.lang.String;
import java.io.FileOutputStream;
import java.io.FileInputStream;

public class DiceNumericalDefinition extends Activity {
	private static void fibmul(Double[] a, Double[] b) {
		double x = a[0]*b[0]+a[1]*b[1];
		double y = a[0]*b[1]+a[1]*(b[0]-b[1]);
		a[0]=x;
		a[1]=y;
	}
	private static Double fib(long n) {
		if (n > 1475)
			return Double.POSITIVE_INFINITY;
		--n;
		Double[] a = new Double[] {1.0,1.0};
		Double[] b = new Double[] {1.0,1.0};
		while (n > 0) {
			if ((n & 1) != 0)
				fibmul(a,b);
			fibmul(b,b);
			n >>= 1;
		}
		return a[0];
	}
	private static Double fact(Long n) {
		if (n > 170)
			return Double.POSITIVE_INFINITY;
		Double res = 1.0;
		for (; n > 1; n--)
			res *= n;
		return res;
	}

	public class Operator implements Cloneable {
		final String str;
		int priority;
		int id;

		Operator(String str, int priority, int id) {
			this.str = str;
			this.priority = priority;
			this.id = id;
		}

		Operator() {
			this.str = "";
			this.priority = -666;
			this.id = -666;
		}

		public Operator clone() throws CloneNotSupportedException {
			return (Operator) super.clone();
		}
	}

	class ExpressionException extends Exception {
		static final long serialVersionUID = 1L;

		ExpressionException(String message) {
			super(message);
		}

	}

	private final Operator[] ops = { new Operator(",", 0, -1), new Operator("||", 1, 1), new Operator("&&", 1, 0),
			new Operator("!=", 2, 5), new Operator("==", 2, 4), new Operator(">=", 2, 3), new Operator("<=", 2, 2),
			new Operator(">", 2, 1), new Operator("<", 2, 0), new Operator("-", -1, 1), new Operator("+", -1, 0),
			new Operator("^", 4, 2), new Operator("/", 4, 1), new Operator("*", 4, 0), new Operator("d", 5, -1),
			new Operator("!", 6, 2), new Operator("fib(", 7, 5), new Operator("fact(", 7, 4),
			new Operator("round(", 7, 3), new Operator("ceil(", 7, 2), new Operator("floor(", 7, 1),
			new Operator("sqrt(", 7, 0), new Operator("min(", -2, 1), new Operator("max(", -2, 0) };

	private static boolean isUnary(Operator op) {
		return op.priority >= 6;
	}

	private final Vector<Operator> lmof = new Vector<Operator>(); //last multiple operator function read 0=max 1=min
	private final Vector<Double> ns = new Vector<Double>(); //number stack
	private final Vector<Operator> os = new Vector<Operator>(); //operator stack
	private Operator tmpo = new Operator();
	private final DecimalFormat df = new DecimalFormat("#0.0");
	private final Random random = new Random();

	private void popOperator() throws ExpressionException {
		if (os.isEmpty())
			throw new ExpressionException("operator stack was empty");
		try {
			tmpo = (os.lastElement().clone());
		} catch (CloneNotSupportedException e) {
			tmpo.id = -666;
			tmpo.priority = -666;
		}
		tmpo.priority %= 8;
		os.remove(os.size() - 1);
		if (ns.isEmpty() || (!isUnary(tmpo) && ns.size() < 2))
			throw new ExpressionException("number stack did not have enough operators");
		Double b = ns.lastElement();
		ns.remove(ns.size() - 1);
		Double a = 0.0;
		if (!isUnary(tmpo)) {
			a = ns.lastElement();
			ns.remove(ns.size() - 1);
		}
		Double err = 0.00001525879;
		if (tmpo.priority == 0) {
			if (tmpo.id == 0)
				ns.add(a > b ? a : b);
			else if (tmpo.id == 1)
				ns.add(a < b ? a : b);
			else
				throw new ExpressionException("unknown operator {" + ((Integer) tmpo.priority).toString() + ", "
						+ ((Integer) tmpo.id).toString() + "} found in stack");
		} else if (tmpo.priority == 1) {
			if (a < 0)
				a = -a;
			if (b < 0)
				b = -b;
			if (tmpo.id == 0)
				ns.add((a < err || b < err) ? 0.0 : 1.0);
			else if (tmpo.id == 1)
				ns.add((a < err && b < err) ? 0.0 : 1.0);
			else
				throw new ExpressionException("unknown operator {" + ((Integer) tmpo.priority).toString() + ", "
						+ ((Integer) tmpo.id).toString() + "} found in stack");
		} else if (tmpo.priority == 2) {
			if (tmpo.id == 0)
				ns.add(a + err < b ? 1.0 : 0.0);
			else if (tmpo.id == 1)
				ns.add(a - err > b ? 1.0 : 0.0);
			else if (tmpo.id == 2)
				ns.add(Math.round(a) <= Math.round(b) ? 1.0 : 0.0);
			else if (tmpo.id == 3)
				ns.add(Math.round(a) >= Math.round(b) ? 1.0 : 0.0);
			else if (tmpo.id == 4)
				ns.add((a > b ? a - b : b - a) < err ? 1.0 : 0.0);
			else if (tmpo.id == 5)
				ns.add((a > b ? a - b : b - a) < err ? 0.0 : 1.0);
			else
				throw new ExpressionException("unknown operator {" + ((Integer) tmpo.priority).toString() + ", "
						+ ((Integer) tmpo.id).toString() + "} found in stack");
		} else if (tmpo.priority == 3) {
			if (tmpo.id == 0)
				ns.add(a + b);
			else if (tmpo.id == 1)
				ns.add(a - b);
			else
				throw new ExpressionException("unknown operator {" + ((Integer) tmpo.priority).toString() + ", "
						+ ((Integer) tmpo.id).toString() + "} found in stack");
		} else if (tmpo.priority == 4) {
			if (tmpo.id == 0)
				ns.add(a * b);
			else if (tmpo.id == 1)
				ns.add(a / b);
			else if (tmpo.id == 2)
				ns.add(Math.pow(a, b));
			else
				throw new ExpressionException("unknown operator {" + ((Integer) tmpo.priority).toString() + ", "
						+ ((Integer) tmpo.id).toString() + "} found in stack");
		} else if (tmpo.priority == 5) {
			if (tmpo.id == 0) {
				int n, m;
				n = (int) Math.round(a);
				m = (int) Math.round(b);
				if (m <= 0)
					throw new ExpressionException(
							"Evaluation error: found die with " + ((Integer) m).toString() + " faces");
				if (n < 0)
					throw new ExpressionException(
							"Evaluation error: cannot throw " + ((Integer) n).toString() + " dice");
				long res = n;
				for (; n > 0; n--) {
					res += random.nextInt(m);
				}
				ns.add((double) res);
			} else
				throw new ExpressionException("unknown operator {" + ((Integer) tmpo.priority).toString() + ", "
						+ ((Integer) tmpo.id).toString() + "} found in stack");
		} else if (tmpo.priority == 6) {
			if (tmpo.id == 0)
				ns.add(b);
			else if (tmpo.id == 1)
				ns.add(-b);
			else if (tmpo.id == 2)
				ns.add((b > -err && b < err) ? 1.0 : 0.0);
			else if (tmpo.id == 3) {
				int m = (int) Math.round(b);
				if (m <= 0)
					throw new ExpressionException(
							"Evaluation error: found die with " + ((Integer) m).toString() + " faces");
				ns.add((double) (random.nextInt(m) + 1));
			} else
				throw new ExpressionException("unknown operator {" + ((Integer) tmpo.priority).toString() + ", "
						+ ((Integer) tmpo.id).toString() + "} found in stack");
		} else if (tmpo.priority == 7) {
			if (tmpo.id == 0)
				ns.add(Math.sqrt(b));
			else if (tmpo.id == 1)
				ns.add(Math.floor(b));
			else if (tmpo.id == 2)
				ns.add(Math.ceil(b));
			else if (tmpo.id == 3)
				ns.add((double) Math.round(b));
			else if (tmpo.id == 4) {
				ns.add(fact(Math.round(b)));
			} else if (tmpo.id == 5) {
				ns.add(fib(Math.round(b)));
			} else
				throw new ExpressionException("unknown operator {" + ((Integer) tmpo.priority).toString() + ", "
						+ ((Integer) tmpo.id).toString() + "} found in stack");
		} else
			throw new ExpressionException("unknown operator {" + ((Integer) tmpo.priority).toString() + ", "
					+ ((Integer) tmpo.id).toString() + "} found in stack");
	}

	private void pushOperator(Operator o) throws ExpressionException {
		while (!os.isEmpty() && (os.lastElement().priority > o.priority
				|| (os.lastElement().priority == o.priority && !isUnary(o)))) {
			popOperator();
		}
		os.add(o);
	}

	private String prepString(String str) throws ExpressionException {
		StringBuilder sb = new StringBuilder();
		int comm = 0;
		int j = 0;
		for (int i = 0; i < str.length(); ++i) {
			if (str.charAt(i) == '[')
				comm++;
			if (comm == 0 && !Character.isWhitespace(str.charAt(i)))
				sb.append(str.charAt(i));
			if (str.charAt(i) == ']') {
				comm--;
				if (comm < 0)
					throw new ExpressionException("Syntax error: unmatched ]");
			}
		}
		return sb.toString();
	}

	private String eval(String in) {
		try {
			in = prepString(in); //remove all whitespace and comments

			//describes last token (priority%8 if operator, -1 if number, -2 if ), -3 if ( )
			int last = 0; //first operator could be unary but cannot be binary
			lmof.clear();
			ns.clear();
			os.clear();
			//parenthesis
			int pc = 0;

			char c;
			int i = 0;
			int j;
			while (i < in.length()) {
				c = in.charAt(i);
				if (c == '(') {
					if (last == -2)
						throw new ExpressionException("Syntax error: found ( after )");
					if (last == -1)
						throw new ExpressionException("Syntax error: found ( after number");
					last = -3;
					pc++;
					i++;
				} else if (c == ')') {
					if (last >= 0)
						throw new ExpressionException("Syntax error: found ) after operator");
					if (last == -3)
						throw new ExpressionException("Syntax error: empty parenthesis");
					last = -2;
					pc--;
					if ((!lmof.isEmpty()) && pc == lmof.lastElement().priority)
						lmof.remove(lmof.size() - 1);
					if (pc < 0) {
						throw new ExpressionException("Syntax error: mismatched ) parenthesis");
					}
					i++;
				} else if (c == '.' || (c >= '0' && c <= '9')) { //number
					if (last == -2)
						throw new ExpressionException("Syntax error: found number after )");
					if (last == -1)
						throw new ExpressionException("Syntax error: found number after number");
					last = -1;
					j = i;
					double res = 0.0;
					boolean dot = false;
					double dm = 0.1;
					while (j < in.length() && (in.charAt(j) == '.' || (in.charAt(j) >= '0' && in.charAt(j) <= '9'))) {
						if (in.charAt(j) == '.') {
							if (dot)
								throw new ExpressionException("Syntax error: cannot have more than 1 '.' in a number");
							dot = true;
						} else {
							if (dot) {
								res += dm * (double) (in.charAt(j) - '0');
								dm /= 10;
							} else {
								res = res * 10.0 + (double) (in.charAt(j) - '0');
							}
						}
						j++;
					}
					ns.add(res);
					i = j;
				} else {
					j = 0;
					for (; j < ops.length; ++j) {
						if (ops[j].str.length() + i <= in.length()) {
							boolean match = true;
							for (int k = 0; k < ops[j].str.length(); ++k) {
								if (ops[j].str.charAt(k) != in.charAt(i + k))
									match = false;
							}
							if (match)
								break;
						}
					}
					if (j >= ops.length)
						throw new ExpressionException("Syntax error: unrecognized operator " + in.charAt(i) + "...");
					try {
						tmpo = (ops[j].clone());
					} catch (Exception e) {
						tmpo.id = -777;
						tmpo.priority = -777;
					}
					if (tmpo.priority == -1) {
						tmpo.priority = (last >= 0 || last == -3) ? 6 : 3;
					}
					if (tmpo.id == -1 && tmpo.priority == 5) {
						tmpo.id = (last >= 0 || last == -3) ? 3 : 0;
						tmpo.priority = (last >= 0 || last == -3) ? 6 : 5;
					}
					if (isUnary(tmpo) || tmpo.priority == -2) {
						if (last == -2)
							throw new ExpressionException("Syntax error: found function or unary operator after )");
						if (last == -1)
							throw new ExpressionException(
									"Syntax error: found function or unary operator after number");
					}
					if (tmpo.priority == -2) { //multiple variable functions
						tmpo.priority = pc;
						lmof.add(tmpo);
						i += tmpo.str.length() - 1;
						continue;
					}
					if ((last >= 0) && !isUnary(tmpo))
						throw new ExpressionException("Syntax error: found binary operator after an operator");
					if ((last == -3) && !isUnary(tmpo))
						throw new ExpressionException("Syntax error: found binary operator after (");
					last = tmpo.priority;
					if (tmpo.priority == 0 && tmpo.id == -1) {//,
						if (lmof.isEmpty() || (pc - 1 != lmof.lastElement().priority))
							throw new ExpressionException(
									"Syntax error: found ',' outside of multiple variable function");
						tmpo.id = lmof.lastElement().id;
					}
					i += tmpo.str.length() - ((tmpo.priority == 7) ? 1 : 0);
					tmpo.priority += pc * 8;
					pushOperator(tmpo);
				}
			}
			if (pc != 0)
				throw new ExpressionException("Syntax error: mismatched parenthesis");
			if (last >= 0)
				throw new ExpressionException("Syntax error: found operator at the end");
			while (!os.isEmpty()) {
				popOperator();
			}
			if (ns.size() != 1)
				throw new ExpressionException("Syntax error: number stack is not 1 number");
			return df.format(ns.get(0));
		} catch (Exception e) {
			return e.getMessage();
		}
	}

	abstract class TextChangedListener<T> implements TextWatcher {
		private final T target;

		TextChangedListener(T target) {
			this.target = target;
		}

		@Override
		public void beforeTextChanged(CharSequence s, int start, int count, int after) {
		}

		@Override
		public void onTextChanged(CharSequence s, int start, int before, int count) {
		}

		@Override
		public void afterTextChanged(Editable s) {
			this.onTextChanged(target);
		}

		protected abstract void onTextChanged(T target);
	}

	private final String efn = "expressions";
	private final Vector<String> list = new Vector<String>();
	private final DisplayMetrics dm = new DisplayMetrics();

	private void loadData() {
		list.clear();
		String[] fl = fileList();
		boolean filePresent = false;
		for (String fn : fl) {
			if (fn.equals(efn))
				filePresent = true;
		}
		if (filePresent) {
			FileInputStream is;
			int i;
			StringBuilder cs = new StringBuilder();
			try {
				is = openFileInput(efn);
				while ((i = is.read()) != -1) {
					if ((char) i == '\n') {
						list.add(cs.toString());
						cs = new StringBuilder();
					} else {
						cs.append((char) i);
					}
				}
				is.close();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}

	private void saveData() {
		StringBuilder fileContents = new StringBuilder();
		for (String string : list) {
			if (string.equals(""))
				continue;
			fileContents.append(string);
			fileContents.append('\n');
		}
		FileOutputStream outputStream;
		try {
			outputStream = openFileOutput(efn, MODE_PRIVATE);
			outputStream.write(fileContents.toString().getBytes());
			outputStream.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void addThing(int i) {
		LinearLayout ll = new LinearLayout(this);
		ll.setId(3 + 3 * i);
		EditText et = new EditText(this);
		et.setId(4 + 3 * i);
		Button b = new Button(this);
		ll.setId(5 + 3 * i);

		et.setTag(i);
		et.setHint("new expression");
		et.setText(list.get(i));
		et.setInputType(InputType.TYPE_TEXT_VARIATION_NORMAL ^ InputType.TYPE_TEXT_FLAG_MULTI_LINE);
		et.setWidth((dm.widthPixels * 3) / 4);
		et.setGravity(Gravity.CENTER_VERTICAL);
		et.addTextChangedListener(new TextChangedListener<EditText>(et) {
			@Override
			public void onTextChanged(EditText v) {
				list.set((Integer) v.getTag(), v.getText().toString());
				if ((Integer) v.getTag() == list.size() - 1 && !v.getText().toString().equals("")) {
					list.add("");
					addThing(list.size() - 1);
				}
			}
		});

		b.setTag(i);
		b.setText("Throw");
		b.setWidth(dm.widthPixels - et.getWidth());
		b.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				((Button) v).setText(eval(list.get((Integer) v.getTag())));
			}
		});

		ll.addView(et);
		ll.addView(b);
		ll.setGravity(Gravity.CENTER);
		((LinearLayout) findViewById(2)).addView(ll);
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		try {
			loadData();
			list.add("");
			getWindowManager().getDefaultDisplay().getMetrics(dm);
			ScrollView scrollView = new ScrollView(this);
			scrollView.setId(1);
			LinearLayout vll = new LinearLayout(this);
			vll.setId(2);
			vll.setOrientation(LinearLayout.VERTICAL);
			scrollView.addView(vll);
			setContentView(scrollView);
			for (int i = 0; i < list.size(); ++i)
				addThing(i);
		} catch (Exception e) {
			EditText textView = new EditText(this);
			textView.setText(e.toString());
			setContentView(textView);
			e.printStackTrace();
		}
	}

	@Override
	protected void onStop() {
		super.onStop();
		saveData();
	}
}
