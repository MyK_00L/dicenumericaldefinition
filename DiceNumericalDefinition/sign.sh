SDK="/opt/android-sdk"
BTOOLS="$SDK/build-tools/34.0.0"
APKSIGNER="$BTOOLS/apksigner"
ZIPALIGN="$BTOOLS/zipalign"

echo "Keystore password: "
read -s kpass

$APKSIGNER sign --ks ../../../ki/Mkeystore.jks --ks-pass pass:$kpass --out build/a2.apk build/a1.apk
#$ZIPALIGN -f 4 bin/final.unaligned.apk bin/final.apk

